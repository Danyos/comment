<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Models\PostComentsReply;
use App\Models\PostComments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {


        if ($request->has('images')) {
            $pah=public_path('/post/'.date("Y") .'/'.date("m"));
            $photos = $request->file('images');
            if(!file_exists($pah)>0):
                if(!is_dir($pah.date("Y").'/'.date("m") )) {
                    mkdir($pah, 0755, true);
                }endif;
            $imageName =date("Y") .'/'.date("m").'/'.rand(1, 999999999999).'.'. $request->images->getClientOriginalExtension();
            $request->images->move(public_path('post/'. date("Y") . '/' . date("m") . '/'), $imageName);
           Post::create([
                'title'=>$request->title,
                'description'=>$request->description,
                'user_id'=>Auth::user()->id,
                'images'=>'/post/'.$imageName,
            ]);
        }else{
            Post::create([
                'title'=>$request->title,
                'description'=>$request->description,
                'user_id'=>Auth::user()->id,

            ]);
        }

return back()->with('succses','thenks add you posting');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts=Post::find($id);
        $comments=PostComments::with('CommentsReply')->where('post_id',$id)->get();;
        return view('user.showPosting',compact('comments','posts','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

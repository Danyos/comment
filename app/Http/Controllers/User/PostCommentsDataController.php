<?php

namespace App\Http\Controllers\User;

use App\Models\PostComentsReply;
use App\Models\PostComments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostCommentsDataController extends Controller
{
    public function addcomments(Request $request){
        if ($request->comment_id==null) {
            $comments = PostComments::create([
                'user_id' => Auth::user()->id,
                'post_id' => $request->post_id,
                'title' => $request->comment,
            ]);
        }
        return back();
    }

    public function repllieadd(Request $request)
    {

        if ($request->comment_id!=null){
            $comments= PostComentsReply::create([
                'user_id'=>Auth::user()->id,
                'post_id'=>$request->post_id,
                'comment_id'=>$request->comment_id,
                'title'=>$request->comment,

            ]);
        }

        return back();

    }
}

<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PostComments extends Model
{
    protected $fillable = [
        'user_id','post_id','title'
    ];
    public function CommentsReply()
    {
        return $this->hasMany(PostComentsReply::class,'comment_id','id');

    }
    public function usercommentname()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}

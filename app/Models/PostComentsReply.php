<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PostComentsReply extends Model
{
    protected $fillable = [
        'user_id','post_id','comment_id','title'
    ];
    public function userpost()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}

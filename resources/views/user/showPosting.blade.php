@extends('layouts.app')
@section('content')
    <section id="blog-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">


                    <aside>
                        <img src="{{asset($posts->images)}}" class="img-responsive" style="height: 200px">
                        <div class="content-title">
                            <div class="text-center">
                                <h3><a href="#">{{$posts->title}}</a></h3>
                            </div>
                        </div>

                        <div class="content-footer">
                            <img class="user-small-img" src="
                                        {{asset('asset/logo/images.jpg')}}">
                            <span style="font-size: 16px;color: #fff;"></span>
                            <span class="pull-right"></span>

                        </div>
                    </aside>
                    <p>{{$posts->description}}</p>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <ul class="comments-list">
                @foreach($comments as $comment)
                    <li class="comment">
                        <a class="" href="#">
                            {{$comment->usercommentname->name}}
                        </a>
                        <div class="comment-body">
                            <div class="comment-heading">
                                <h4> {{$comment->title}}</h4>
                                <h5 class="time">{{$comment->created_at}}</h5>
                            </div>
                            <a href="#" class="btn btn-default stat-item">
                                <i class="fa fa-share icon"></i>{{count($comment->CommentsReply)}}
                            </a>
                        </div>
                        <ul class="comments-list">
                            @foreach($comment->CommentsReply as $commentsreply)

                                <li class="comment-{{$comment->id}}">
                                    <a class="" href="#">
                                        {{$commentsreply->userpost->name}}
                                    </a>
                                    <div class="comment-body">
                                        <div class="comment-heading">
                                            <h4 class="user">{{$commentsreply->title}}</h4>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            <form method="post" action="{{route('user.repllieaddurl')}}">
                                @csrf
                                <input class="form-control comentinput" name="comment" placeholder="Add a comment"
                                       type="text">
                                <input type="hidden" value="{{$id}}" class="form-control" name="post_id">
                                <input type="hidden" value="{{$comment->id}}" class="form-control" name="comment_id">
                                <span class="input-group-addon">
                        <button type="submit"><i class="fa fa-plus"></i></button>
                    </span>
                            </form>
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>


        <form class="commentform" method="post" action="{{route('user.commentsADD')}}">
            @csrf
            <input class="form-control comentinput" name="comment" placeholder="Add a comment" type="text">
            <input type="hidden" value="{{$id}}" class="form-control" name="post_id">
            <input type="hidden" value="{{null}}" class="form-control" name="comment_id">
            <span class="input-group-addon">

         <button type="submit"><i class="fa fa-plus"></i></button>
        </span>
        </form>
    </section>

@endsection



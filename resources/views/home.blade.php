@extends('layouts.app')

@section('content')
               <section id="blog-section" >

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    @foreach($post as $posting)
                                    <div class="col-lg-6 col-md-6">
                                        <aside onclick="location.href='{{route('user.post.show',$posting->id)}}'">
                                            <img src="{{asset($posting->images)}}" class="img-responsive" style="height: 200px">
                                            <div class="content-title">
                                                <div class="text-center">
                                                    <h3><a href="#">{{$posting->title}}</a></h3>
                                                </div>
                                            </div>
                                            <div class="content-footer">
                                                <img class="user-small-img" src="{{asset('asset/logo/images.jpg')}}">
                                                <span style="font-size: 16px;color: #fff;">{{auth()->user()->name}}r</span>
                                                <span class="pull-right"></span>

                                            </div>
                                        </aside>
                                    </div>

                          @endforeach
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="widget-sidebar">
                                    <h2 class="title-widget-sidebar">// Add new Post</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ut .</p>
                                    <form method="post" action="{{route('user.poststore')}}" enctype="multipart/form-data">
                                        @csrf
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-title-o" aria-hidden="true">TITLE</i></span>
                                        <input id="" type="text" class="form-control" name="title" value="{{old('title')}}">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-clipboard" aria-hidden="true"></i></span>
                                        <textarea name="description" id="description"  class="form-control">{{old('description')}}</textarea>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-title-o" aria-hidden="true">Images</i></span>
                                        <input type="file" class="form-control" onchange="return previewimage(event)"  name="images">
                                    </div>
                                    <div class="input-group">
                                        <img src="{{asset('asset/logo/images.jpg')}}" alt="" onclick="$('#okimage').trigger('click')" id="output" style="position: relative" class="img-responsive">
                                    </div>
                                    <button type="submit" class="btn btn-warning">Warning</button>
                                    </form>
                                </div>

                            </div>


                        </div>
                    </div>

               </section>
                    @endsection

@section('js')
    <script>
        function previewimage(event) {
            var output=document.getElementById('output');
            output.src=URL.createObjectURL(event.target.files[0]);
        }
    </script>

@endsection

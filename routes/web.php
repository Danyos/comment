<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','login');
Auth::routes();
Route::group(['as' => 'user.', 'namespace' => 'User', 'middleware' => 'auth'], function () {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/logout', 'HomeController@logout')->name('logout');
    Route::any('/post/comment/Add', 'PostCommentsDataController@addcomments')->name('commentsADD');
    Route::resource('post', 'PostController');
    Route::post('post/add', 'PostController@store')->name('poststore');

    Route::resource('comments', 'CommentsController');

    Route::post('post/add/replli', 'PostCommentsDataController@repllieadd')->name('repllieaddurl');


});
